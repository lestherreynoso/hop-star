﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HopStarBunnyJump : MonoBehaviour {
    public static HopStarBunnyJump instance;


    private Rigidbody2D myBody;
    private Animator anim;

    [SerializeField]
    private float forceX, forceY;

    private float tresholdX = 7f;
    private float tresholdY = 14f;

    private bool setPower, didJump;

    private Slider powerBar;
    private float powerBarTreshold = 10f;
    private float powerBarValue = 0f;
    private GameObject currentPlatform;

    void Awake()
    {
        MakeInstance();
        Initiliaze();
    }

    void Update()
    {
        SetPower();
    }

    void Initiliaze()
    {
        powerBar = GameObject.Find("Power Bar").GetComponent<Slider>();
        myBody = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        powerBar.minValue = 0f;
        powerBar.maxValue = 10f;
        powerBar.value = powerBarValue;

    }

    void MakeInstance()
    {
        if (instance == null)
            instance = this;
    }

    void SetPower()
    {

        if (setPower)
        {
            forceX += tresholdX * Time.deltaTime;
            forceY += tresholdY * Time.deltaTime;

            if (forceX > 8f)
                forceX = 8f;

            if (forceY > 15f)
                forceY = 15f;

           powerBarValue += powerBarTreshold * Time.deltaTime;
           powerBar.value = powerBarValue;

        }

    }

    public void SetPower(bool setPower)
    {
        this.setPower = setPower;

        if (!setPower)
        {
            Jump();
        }
    }

    void Jump()
    {
        myBody.velocity = new Vector2(forceX, forceY);
        forceX = forceY = 0f;
        didJump = true;

        anim.SetBool("Jump", didJump);

        powerBarValue = 0f;
        powerBar.value = powerBarValue;
    }

    void OnTriggerEnter2D(Collider2D target)
    {

        if (didJump)
        {
            didJump = false;

            anim.SetBool("Jump", didJump);

            if (target.name.Contains("Platform"))
            {
                
                if (HopStarGameManager.instance != null && currentPlatform != target.gameObject)
                {
                    currentPlatform = target.gameObject;
                    HopStarGameManager.instance.CreateNewPlatformAndLerp(target.transform.position.x);
                }

                if (HopStarScoreManager.instance != null)
                {
                    HopStarScoreManager.instance.IncrementScore();
                }

            }
        }

        if (target.name.Contains("Dead"))
        {
            if (HopStarGameOverManager.instance != null)
            {
                HopStarGameOverManager.instance.GameOverShowPanel();
            }
            Destroy(gameObject);
        }
    }

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class HopStarJumpButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public void OnPointerDown(PointerEventData data)
    {
        if (HopStarBunnyJump.instance != null)
        {
            HopStarBunnyJump.instance.SetPower(true);
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        if (HopStarBunnyJump.instance != null)
        {
            HopStarBunnyJump.instance.SetPower(false);
        }
    }
}

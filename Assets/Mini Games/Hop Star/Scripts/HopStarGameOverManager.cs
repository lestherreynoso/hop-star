﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;


public class HopStarGameOverManager : MonoBehaviour {
    public static HopStarGameOverManager instance;

    private GameObject gameOverPanel;
    private Animator gameOverAnim;

    private Button playAgainBtn, backBtn;

    private GameObject scoreText;
    private Text finalScore;

    void Awake()
    {
        MakeInstance();
        InitializeVariables();
    }

    void MakeInstance()
    {
        if (instance == null)
            instance = this;
    }

    public void GameOverShowPanel()
    {
        scoreText.SetActive(false);
        gameOverPanel.SetActive(true);

        finalScore.text = "Score \n" + HopStarScoreManager.instance.GetScore();

        gameOverAnim.Play("Fade In");
    }

    void InitializeVariables()
    {
        gameOverPanel = GameObject.Find("Game Over Panel Holder");
        gameOverAnim = gameOverPanel.GetComponent<Animator>();

        playAgainBtn = GameObject.Find("Restart Button").GetComponent<Button>();
        backBtn = GameObject.Find("Back Button").GetComponent<Button>();

        playAgainBtn.onClick.AddListener(() => PlayAgain());
        backBtn.onClick.AddListener(() => BackToMenu());

        scoreText = GameObject.Find("Score Text");
        finalScore = GameObject.Find("Final Score").GetComponent<Text>();

        gameOverPanel.SetActive(false);
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void BackToMenu()
    {
        SceneFader.instance.LoadLevel("Hop Star Title Screen");
    }
}

﻿using UnityEngine;

using System.Collections;

public class HopStarMainMenuController : MonoBehaviour {
    public void PlayGame()
    {
        SceneFader.instance.LoadLevel("Hop Star Gameplay");
    }
}

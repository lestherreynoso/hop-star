﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HopStarScoreManager : MonoBehaviour {

    public static HopStarScoreManager instance;

    private Text scoreText;

    private int score;

    void Awake()
    {
        scoreText = GameObject.Find("Score Text").GetComponent<Text>();
        MakeInstance();
    }

    void MakeInstance()
    {
        if (instance == null)
            instance = this;
    }

    public void IncrementScore()
    {
        score++;
        scoreText.text = "" + score;
    }

    public int GetScore()
    {
        return this.score;
    }
}

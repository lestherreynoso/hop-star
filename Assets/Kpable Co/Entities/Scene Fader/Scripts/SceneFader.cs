﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneFader : MonoBehaviour {
    public static SceneFader instance;

    [SerializeField]
    private GameObject fadePanel;

    [SerializeField]
    private Animator anim;
	// Use this for initialization
	void Awake () {
        MakeSingleton();
    }
    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
	
	// Update is called once per frame
	public void LoadLevel (string level) {
        StartCoroutine(FadeInOut(level));
	}

    IEnumerator FadeInOut(string level)
    {
        fadePanel.SetActive(true);
        anim.Play("FadeOut");
        yield return StartCoroutine(WaitForRealSeconds(1f));
        SceneManager.LoadScene(level);
        anim.Play("FadeIn");

        yield return StartCoroutine(WaitForRealSeconds(.7f));

        fadePanel.SetActive(false);
    }

    IEnumerator WaitForRealSeconds(float time)
    {
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < (start + time))
        {
            yield return null;
        }
    }
}
